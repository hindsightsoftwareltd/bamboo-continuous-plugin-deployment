package com.atlassian.bamboo.plugins.confdeploy;

public class FecruDeployTask extends AutoDeployTask
{
    @Override
    public Product getTargetProduct()
    {
        return Product.FECRU;
    }
}
