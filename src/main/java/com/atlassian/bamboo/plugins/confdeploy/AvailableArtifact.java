package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;

/**
 * Encapsulates a reference to an {@link com.atlassian.bamboo.plan.artifact.ArtifactDefinition} to the task edit screen.
 */
public class AvailableArtifact
{
    private final ArtifactDefinition wrappedDefinition;

    AvailableArtifact(ArtifactDefinition wrappedDefinition)
    {
        this.wrappedDefinition = wrappedDefinition;
    }

    public String getName()
    {
        return wrappedDefinition.getName();
    }

    public long getId()
    {
        return wrappedDefinition.getId();
    }

    @Override
    public String toString()
    {
        return getName();
    }
}
